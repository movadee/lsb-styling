import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isActive = false;
  selectedItem = 'CA-EN';
  radioModel = 'No';

  provinceList = [
    {value: "ca-en", viewValue: "CA-EN"},
    {value: "ca-fr", viewValue: "CA-FR"}
  ];

  listClick(event, newValue) {
    this.selectedItem = newValue;
  }

  // // datepicker
  // minDate = new Date(2015, 5, 10);
  // maxDate = new Date(2018, 9, 15);
 
  // bsValue: Date = new Date();
  // bsRangeValue: any = [new Date(2017, 7, 4), new Date(2017, 7, 20)];
}
